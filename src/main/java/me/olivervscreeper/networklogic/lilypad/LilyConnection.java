package me.olivervscreeper.networklogic.lilypad;

import lilypad.client.connect.api.Connect;
import lilypad.client.connect.api.request.RequestException;
import lilypad.client.connect.api.request.impl.MessageRequest;
import lilypad.client.connect.api.request.impl.RedirectRequest;
import lilypad.client.connect.api.result.FutureResult;
import lilypad.client.connect.api.result.FutureResultListener;
import lilypad.client.connect.api.result.StatusCode;
import lilypad.client.connect.api.result.impl.MessageResult;
import lilypad.client.connect.api.result.impl.RedirectResult;
import me.olivervscreeper.networklogic.PluginUtils;
import org.bukkit.entity.Player;

import java.io.UnsupportedEncodingException;

/**
 * Created on 11/10/2014.
 *
 * @author OliverVsCreeper
 */
public class LilyConnection {

    public static boolean success = false;

    public static Connect getBukkitConnect() {
        try {
            return (Connect) PluginUtils.pl.getServer().getServicesManager().getRegistration(Connect.class).getProvider();
        }catch(Exception ex){
            return null;
        }
    }

    public static Boolean teleportToServer(String server, final Player player) {
        try {
            success = false;
            //create connection
            Connect c = getBukkitConnect();
            if(c == null){
                return false;
            }
            //new RedirectRequest to transfer the player
            c.request(new RedirectRequest(server, player.getName())).registerListener(new FutureResultListener<RedirectResult>() {
                //listen for a successful transfer
                public void onResult(RedirectResult redirectResult) {
                    if (redirectResult.getStatusCode() == StatusCode.SUCCESS) {
                        success = true;
                        return;
                    }
                }
            });
            if(success){
                return true;
            }else{
                return false;
            }
        } catch (Exception exception) {
            return false;
        }
    }

    //API Methods

    public static void sendGlobalLilyMessage(String message, String channel) {
        Connect c = getBukkitConnect();
        MessageRequest request = null;
        try {
            request = new MessageRequest("", channel, message); //channelName (short), message
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        FutureResult<MessageResult> futureResult = null;
        try {
            futureResult = c.request(request);
        } catch (RequestException e) {
            e.printStackTrace();
        }

    }

    public static void sendPrivateLilyMessage(String message, String channel, String server) {
        Connect c = getBukkitConnect();
        MessageRequest request = null;
        try {
            request = new MessageRequest(server, channel, message); //channelName (short), message
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        FutureResult<MessageResult> futureResult = null;
        try {
            futureResult = c.request(request);
        } catch (RequestException e) {
            e.printStackTrace();
        }

    }

}
