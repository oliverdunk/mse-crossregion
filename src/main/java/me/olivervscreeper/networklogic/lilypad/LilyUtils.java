package me.olivervscreeper.networklogic.lilypad;

import me.olivervscreeper.networklogic.PluginUtils;
import org.bukkit.Bukkit;

/**
 * Created on 17/10/2014.
 *
 * @author OliverVsCreeper
 */
public class LilyUtils {

    public static void pingNetwork(){
        LilySync.maxSlots.clear();
        LilySync.connectionsCount.clear();
        PluginUtils.pl.getLogger().info("Pinging servers...");
        try{
            LilyConnection.sendGlobalLilyMessage("ping", "ping");
        }catch(Throwable ex){
            PluginUtils.pl.getLogger().info("Couldn't ping network.");
        }
    }

    public static Boolean isFull(String server){
        if(!LilySync.maxSlots.containsKey(server)) return true;
        if(LilySync.connectionsCount.get(server) >= LilySync.maxSlots.get(server)) return true;
        return false;
    }

    public static void sendInfo(){
        LilyConnection.sendGlobalLilyMessage("" + Bukkit.getServer().getOnlinePlayers().size(), "slots");
        LilyConnection.sendGlobalLilyMessage("" + Bukkit.getServer().getMaxPlayers(), "maxslots");
    }

}
