package me.olivervscreeper.networklogic.lilypad;

import com.kill3rtaco.tacoserialization.PlayerSerialization;
import lilypad.client.connect.api.event.EventListener;
import lilypad.client.connect.api.event.MessageEvent;
import me.olivervscreeper.networklogic.PluginUtils;
import me.olivervscreeper.networklogic.worldguard.WGAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created on 15/10/2014.
 *
 * @author OliverVsCreeper
 */
public class LilySync implements Listener{

    public static HashMap<String, Integer> maxSlots = new HashMap<String, Integer>();
    public static HashMap<String, Integer> connectionsCount = new HashMap<String, Integer>();

    public static HashMap<String, Location> playerLocations = new HashMap<String, Location>();
    public static HashMap<String, String> playerInventories = new HashMap<String, String>();

    public static List<String> servers = new ArrayList<String>();



    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        if (playerInventories.containsKey(event.getPlayer().getName())) {
            PlayerSerialization.setPlayer(playerInventories.get(event.getPlayer().getName()), event.getPlayer());
            playerInventories.remove(event.getPlayer().getName());
        }else{
            PlayerSerialization.setPlayer(PluginUtils.jedis.get(event.getPlayer().getName()), event.getPlayer());
        }
        LilyUtils.sendInfo();
        if (!playerLocations.containsKey(event.getPlayer().getName())) return;
        event.getPlayer().teleport(playerLocations.get(event.getPlayer().getName()));
        playerLocations.remove(event.getPlayer().getName());
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        LilyUtils.sendInfo();
        WGAPI.needsTeleport.remove(event.getPlayer().getName());
        PluginUtils.jedis.set(event.getPlayer().getName(), PlayerSerialization.serializePlayerAsString(event.getPlayer()));
    }

    @EventListener
    public void onMessage(MessageEvent me) {

        String channel = me.getChannel();
        String sender = me.getSender(); //gets the server it was sent from

        try{
            switch (channel){
                case "ping":
                    requestInfo(sender);
                    LilyConnection.sendPrivateLilyMessage("ping", "ping", sender);
                    if(servers.contains(sender)) return;
                    servers.add(sender);
                    break;
                case "request":
                    LilyUtils.sendInfo();
                    break;
                case "slots":
                    connectionsCount.remove(sender);
                    connectionsCount.put(sender, Integer.parseInt(me.getMessageAsString()));
                    break;
                case "maxslots":
                    maxSlots.remove(sender);
                    maxSlots.put(sender, Integer.parseInt(me.getMessageAsString()));
                    break;
                case "location":
                    String messageAsString = me.getMessageAsString();
                    final String NAME = new String(messageAsString).split(":")[0];
                    String locString = new String(messageAsString).split(":")[1];

                    String[] strar = locString.split(";");
                    final Location NEWLOC = new Location(Bukkit.getWorld(strar[0]), Double.valueOf(strar[1]).doubleValue(), Double.valueOf(strar[2]).doubleValue(), Double.valueOf(strar[3]).doubleValue(), Float.valueOf(strar[4]).floatValue(), Float.valueOf(strar[5]).floatValue());

                    playerLocations.put(NAME, NEWLOC);
                    PluginUtils.pl.getLogger().info("Received location of player " + NAME + "!");
                    break;
                case "inventory":
                    String message = me.getMessageAsString();
                    final String USER = new String(message).split("!#!")[0];
                    String inv = new String(message).split("!#!")[1];

                    playerInventories.put(USER, inv);
                    PluginUtils.pl.getLogger().info("Received inventory of player " + USER + "!");
                    break;
            }
        }catch(Exception ex){ex.printStackTrace();}
    }

    public static void requestInfo(String server){
        LilyConnection.sendPrivateLilyMessage("slots", "request", server);
    }


}
