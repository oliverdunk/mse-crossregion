package me.olivervscreeper.networklogic.worldguard;

import com.kill3rtaco.tacoserialization.PlayerSerialization;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import me.olivervscreeper.networklogic.BukkitUtils;
import me.olivervscreeper.networklogic.PluginUtils;
import me.olivervscreeper.networklogic.lilypad.LilyConnection;
import me.olivervscreeper.networklogic.lilypad.LilySync;
import me.olivervscreeper.networklogic.lilypad.LilyUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created on 02/10/2014.
 *
 * @author OliverVsCreeper
 */
public class WGAPI implements Listener{

    WorldGuardPlugin plugin;
    public static HashMap<String, String> needsTeleport = new HashMap<String, String>();

    public WGAPI(){
        plugin = getWorldGuard();
    }

    private WorldGuardPlugin getWorldGuard() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");

        // WorldGuard may not be loaded
        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
            return null; // Maybe you want throw an exception instead
        }

        return (WorldGuardPlugin) plugin;
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event){
        //Ignore if player hasn't moved a full block.
        if(event.getTo().getBlockX() == event.getFrom().getBlockX() &&
                event.getTo().getBlockY() == event.getFrom().getBlockY() &&
                event.getTo().getBlockZ() == event.getFrom().getBlockZ()) return;

        RegionManager rm = plugin.getRegionManager(event.getPlayer().getWorld());
            ApplicableRegionSet regions = rm.getApplicableRegions(event.getPlayer().getLocation());
                ProtectedRegion region = getHighestRegion(regions);
                if(region == null) return;
                if (region.getId().contains("switch-")){
                    if(needsTeleport.containsKey(event.getPlayer().getName())){
<<<<<<< HEAD
						if(region.getId().equals(needsTeleport.get(event.getPlayer().getName()))){
							return;
						}else{
							needsTeleport.remove(event.getPlayer().getName());
						}
					}
                        if(needsTeleport.get(event.getPlayer().getName()).equals(region.getId())) return;
                        needsTeleport.remove(event.getPlayer().getName());
                    }
                    if((region.getId().split("-")[1]).equals(LilyConnection.getBukkitConnect().getSettings().getUsername())) return;
                    PluginUtils.debug(event.getPlayer() , "Registered teleport request.");
                    processRegionForPlayer(region, event.getPlayer());
                }
            }

    public ProtectedRegion getHighestRegion(ApplicableRegionSet regions){
        int highestFound = -1;
        Iterator I;
        I = regions.iterator();
        while (I.hasNext()) { //Search for highest region
            ProtectedRegion region = (ProtectedRegion) I.next();
            if((!region.getId().contains("switch-")) && (!region.getId().contains("lobby#"))) continue;
            if(region.getPriority() <= highestFound) continue;
            highestFound = region.getPriority();
        }
        I = regions.iterator();
        while (I.hasNext()) { //Get highest region that was detected
            ProtectedRegion region = (ProtectedRegion) I.next();
            if((!region.getId().contains("switch-")) && (!region.getId().contains("lobby#"))) continue;
            if(region.getPriority() == highestFound) return region;
        }
        return null;
    }

    private void processRegionForPlayer(ProtectedRegion region, Player player) {
        if(!region.getId().contains("switch-")) return; //Region is a teleporter?
        String[] args = region.getId().split("-");
        Iterator I = LilySync.servers.iterator();
        String server = "";
        while(I.hasNext()){
            String name = (String) I.next();
            server = server + name;
        }
        if(!server.contains(args[1])) return;

        requestTransfer(player, args[1], Integer.parseInt(args[2]), 0);
    }

    private static void requestTransfer(Player player, String server, int max, int current){
        if(max <= current) {
            needsTeleport.put(player.getName(), 1 + "/" + max + "/" + server);
        }else{
            needsTeleport.put(player.getName(), (current + 1) + "/" + max + "/" + server);
        }
    }

    public static void attemptTeleports(){
        for(String name : needsTeleport.keySet()){

            if(Bukkit.getPlayer(name) == null){
                needsTeleport.remove(name);
                continue;
            }

            String[] variables = needsTeleport.get(name).split("/");
            int attemptServer = Integer.parseInt(variables[0]);
            int maxServers = Integer.parseInt(variables[1]);
            String serverName = variables[2];

            Location l = Bukkit.getPlayer(name).getLocation();

            PluginUtils.debug(Bukkit.getPlayer(name), "Attempting to send you to " + serverName + " ID: " + attemptServer);
            if(!LilyUtils.isFull(serverName + "-" + attemptServer)) { //If server is not full
                sendLocation(name, l, serverName + attemptServer);
                sendInventory(Bukkit.getPlayer(name), serverName + attemptServer);
                Boolean success = LilyConnection.teleportToServer(serverName + "-" + attemptServer, Bukkit.getPlayer(name));
                if (!success){
                    requestTransfer(Bukkit.getPlayer(name), serverName, maxServers, attemptServer);
                    PluginUtils.debug(Bukkit.getPlayer(name), "Error connecting to " + serverName + attemptServer);
                }else {
                    needsTeleport.remove(name);
                }
                continue;
            }
                PluginUtils.debug(Bukkit.getPlayer(name), ChatColor.RED + "Server is logged, and is full.");
                requestTransfer(Bukkit.getPlayer(name), serverName, maxServers, attemptServer);
                continue;
        }
    }

    private static void sendLocation(String name, Location l, String server) {
        LilyConnection.sendPrivateLilyMessage(name + ":" + BukkitUtils.locationToString(l), "location", server);
    }

    private static void sendInventory(Player p, String server){
        LilyConnection.sendPrivateLilyMessage(p.getName() + "!#!" + PlayerSerialization.serializePlayerAsString(p), "inventory", server);
    }

}
