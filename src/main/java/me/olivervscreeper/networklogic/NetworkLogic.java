package me.olivervscreeper.networklogic;

import me.olivervscreeper.networklogic.lilypad.LilySync;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created on 02/10/2014.
 *
 * @author OliverVsCreeper
 */
public class NetworkLogic extends JavaPlugin{

    public void onEnable(){

        //Save class instances to static fields
        PluginUtils.pl = this;
        PluginUtils.sync = new LilySync();

        //Setup plugin and announce licence in console
        PluginUtils.outputInformation();
        PluginUtils.setupPlugin();

    }



}
