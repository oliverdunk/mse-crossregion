package me.olivervscreeper.networklogic;

import org.bukkit.Location;

/**
 * Created on 21/10/2014.
 *
 * @author OliverVsCreeper
 */
public class BukkitUtils {

    public static String locationToString(Location loc) {
        return loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ() + ";" + loc.getYaw() + ";" + loc.getPitch();
    }

}
