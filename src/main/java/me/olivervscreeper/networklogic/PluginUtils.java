package me.olivervscreeper.networklogic;

import me.olivervscreeper.networklogic.commands.ServerList;
import me.olivervscreeper.networklogic.lilypad.LilyConnection;
import me.olivervscreeper.networklogic.lilypad.LilySync;
import me.olivervscreeper.networklogic.lilypad.LilyUtils;
import me.olivervscreeper.networklogic.worldguard.WGAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import redis.clients.jedis.Jedis;

/**
 * Created on 17/10/2014.
 *
 * @author OliverVsCreeper
 */
public class PluginUtils {

    public static Boolean debug = true;
    public static Plugin pl;
    public static LilySync sync;
    public static Jedis jedis;

    //Sends a message to a player with debug formatting, if they are shifting.
    public static void debug(Player player, String message){
        if(!player.isSneaking()) return; //Send DEBUG message if sneaking.
        if(!player.hasPermission("teleports.debug")) return;
        player.sendMessage(ChatColor.BLACK + "[DEBUG] " + ChatColor.GRAY + message);
    }

    //Outputs plugin copyright information to the console.
    public static void outputInformation() {
        pl.getLogger().info("NetworkLogic Initialized. [VERSION " + pl.getDescription().getVersion() + "]");
        pl.getLogger().info("This version licenced to MineconSE 2014.");
        pl.getLogger().info("Not for distribution.");
    }

    //Registers plugin
    public static void setupPlugin() {
        pl.saveDefaultConfig();
        String[] args = pl.getConfig().getString("redis").split(":");
        if(args.length == 1) {
            jedis = new Jedis(args[0]);
        }else{
            jedis = new Jedis(args[0], Integer.parseInt(args[1]));
        }
        Bukkit.getPluginManager().registerEvents(new WGAPI(), pl);
        Bukkit.getPluginManager().registerEvents(new LilySync(), pl);
        LilyConnection.getBukkitConnect().registerEvents(sync);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, //Attempt to refresh servers every minute
                new Runnable(){
                    public void run(){
                        LilyUtils.pingNetwork();
                    }
                }, 100, 1200);


        Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, //Attempt to perform teleports every second.
                new Runnable() {
                    @Override
                    public void run() {
                        WGAPI.attemptTeleports();
                    }
                }, 0, 20);

        Bukkit.getPluginCommand("serverlist").setExecutor(new ServerList());
    }
}
