package me.olivervscreeper.networklogic.commands;

import me.olivervscreeper.networklogic.lilypad.LilySync;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Created on 19/10/2014.
 *
 * @author OliverVsCreeper
 */
public class ServerList implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(command.getLabel().equals("serverlist")){
            if(commandSender.hasPermission("networklogic.serverlist")){
                commandSender.sendMessage(ChatColor.AQUA + "Available Servers:");
                for(String server : LilySync.servers){
                    commandSender.sendMessage(ChatColor.GOLD + "- " + server + " (" + LilySync.connectionsCount.get(server) + "/" + LilySync.maxSlots.get(server) + ")" );
                }
            }else{
                commandSender.sendMessage(ChatColor.RED + "Uh oh! You can't go saying that!");
            }
        }
        return true;
    }
}
