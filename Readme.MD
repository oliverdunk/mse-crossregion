NetworkLogic

What is it?
----------

NetworkLogic is a fast, powerful and efficient server utility for connecting Minecraft servers.<br>
Originally designed to handle data sync and manage players efficiently at the MSE event in 2014,<br>
it is growing into a flexible utility and API for both server owners and developers alike which<br>
will allow reliable connectivity for many servers in the future.



Installation
----------

In its current state, NetworkLogic has no configuration or setup. Simply drop the plugin into<br>
LilyPad servers which have been connected, and the software will automatically establish<br>
a link. Further configuration is planned in the future.

Usage
----------

Cross-Network-Inventories:<br>
Simply enter Redis server information into the configuration file.

Location based teleports:<br>
Simply create WorldGuard regions in the format "switch-NAME-count". Up until the count value,<br>
the plugin will attempt to connect you to "NAMEcount" etc. Locations are synced automatically<br>
using cross server networking.



Dependencies
----------
<br>
The following software is required at runtime:<br>
- WorldEdit<br>
- WorldGuard<br>
- Bukkit-Connect<br>



Author
----------

Developed by @OliverVsCreeper - devATolivervscreeper.co.uk